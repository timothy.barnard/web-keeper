//
//  SwiftyNetworking.h
//  SwiftyNetworking
//
//  Created by Timothy on 13/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SwiftyNetworking.
FOUNDATION_EXPORT double SwiftyNetworkingVersionNumber;

//! Project version string for SwiftyNetworking.
FOUNDATION_EXPORT const unsigned char SwiftyNetworkingVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwiftyNetworking/PublicHeader.h>


