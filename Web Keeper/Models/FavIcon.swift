//
//  FavIcon.swift
//  Web Keeper
//
//  Created by Timothy on 17/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

struct FavIcon: Codable {
    let url: String
    let width: Int
    let height: Int
    let fileType: String
    
    enum CodingKeys: String, CodingKey {
        case fileType = "file_type"
        case url, width, height
    }
}
