//
//  WebCollectionViewCell.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

class WebCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setupCell(webPage: WebPage) {
        self.titleLabel.text = webPage.title
        self.imageView.image = webPage.image ?? #imageLiteral(resourceName: "empty-webpage")
    }
}
