//
//  MainContract.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

protocol MainView {
    // TODO: Declare view methods
}

protocol MainPresentation: class {
    func viewDidLoad()
    func viewDidAppear()
    func openPage(_ webPage: WebPage)
}

protocol MainUseCase: class {
    // TODO: Declare use case methods
}

protocol MainInteractorOutput: class {
    // TODO: Declare interactor output methods
}

protocol MainWireframe: class {
    func showWebPage(_ webPage: WebPage)
}
