//
//  MainPresenter.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

class MainPresenter {

    // MARK: Properties
    var view: MainView?
    var router: MainWireframe?
    var interactor: MainUseCase?
}

extension MainPresenter: MainPresentation {
    
    func viewDidLoad() {
    }
    
    func viewDidAppear() {
    }
    
    func openPage(_ webPage: WebPage) {
        self.router?.showWebPage(webPage)
    }
}

extension MainPresenter: MainInteractorOutput {
    // TODO: implement interactor output methods
}
