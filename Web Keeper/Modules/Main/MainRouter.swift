//
//  MainRouter.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit

class MainRouter {

    // MARK: Properties
    weak var viewController: UIViewController?

    // MARK: Static methods
    static func assembleModule() -> MainViewController {
        let view = MainViewController()
        let presenter = MainPresenter()
        let router = MainRouter()
        let interactor = MainInteractor()

        //let navigation = UINavigationController(rootViewController: view!)

        view.presenter =  presenter

        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor

        interactor.output = presenter
        
        router.viewController = view

        //return navigation

        return view
    }
}

extension MainRouter: MainWireframe {
    
    func showWebPage(_ webPage: WebPage) {
        let webViewController = WebRouter.assembleModule(webPage)
        self.viewController?.present(webViewController, animated: true, completion: nil)
    }
}
