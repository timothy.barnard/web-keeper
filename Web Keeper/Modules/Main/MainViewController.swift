//
//  MainViewController.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit
import SwiftyExtensions

class MainViewController: UIViewController {
    
    // MARK: Properties
    var webPages: [WebPage] = [] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let width = (self.view.frame.width / 2) - 10
        layout.itemSize = CGSize(width: width, height: width)
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.allowsSelection = true
        cv.backgroundColor = .white
        cv.register(WebCollectionViewCell.self)
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    var presenter: MainPresentation?

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        presenter?.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.viewDidAppear()
    }

    private func setupView() {
        self.newObject()
        self.title = "Web Keeper"
        self.view.addSubview(self.collectionView)
        NSLayoutConstraint.activate([
            self.collectionView.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
        ])
    }
    
    private func newObject() {
//        let newModel = WebPage()
//        newModel.title = "Facebook"
//        newModel.url = "https://facebook.com"
//        newModel.imagePath = "https://www.facebook.com/images/fb_icon_325x325.png"
        let cloudmanger = CloudKitManager()
        //cloudmanger.saveNewObject(newModel, name: WebPage.name, blobURL: newModel.imagePath)
        cloudmanger.getAllObjects(WebPage.name) { (pages) in
            if let pages = pages as? [WebPage] {
                self.webPages = pages
            }
        }
    }
}

// MARK: MainView
extension MainViewController: MainView {
    // TODO: implement view output methods
}

// MARK: UICollectionViewDelegate
extension MainViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter?.openPage(self.webPages[indexPath.row])
    }
}

// MARK: UICollectionViewDataSource
extension MainViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.webPages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return sectionItemCell(collectionView, cellForItemAt: indexPath as NSIndexPath)
    }
    
    func sectionItemCell(_ collectionView: UICollectionView, cellForItemAt indexPath: NSIndexPath ) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath as IndexPath) as WebCollectionViewCell
        cell.setupCell(webPage: self.webPages[indexPath.row])
        return cell
    }

}
