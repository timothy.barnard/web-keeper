//
//  WebContract.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

protocol WebView {
    func showURL(_ urlString: String)
}

protocol WebPresentation: class {
    func viewDidLoad()
    func viewDidAppear()
    // TODO: Declare presentation methods
}

protocol WebUseCase: class {
    // TODO: Declare use case methods
}

protocol WebInteractorOutput: class {
    // TODO: Declare interactor output methods
}

protocol WebWireframe: class {
    // TODO: Declare wireframe methods
}
