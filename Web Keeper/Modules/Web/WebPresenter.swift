//
//  WebPresenter.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

class WebPresenter {

    // MARK: Properties
    var view: WebView?
    var router: WebWireframe?
    var interactor: WebUseCase?
    
    var webPage: WebPage?
}

extension WebPresenter: WebPresentation {
    
    func viewDidLoad() {
    }
    
    func viewDidAppear() {
        if let page = self.webPage {
            self.view?.showURL(page.url)
        }
    }
}

extension WebPresenter: WebInteractorOutput {
    // TODO: implement interactor output methods
}
