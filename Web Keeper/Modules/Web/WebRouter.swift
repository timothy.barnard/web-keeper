//
//  WebRouter.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit

class WebRouter {

    // MARK: Properties
    weak var viewController: UIViewController?

    // MARK: Static methods
    static func assembleModule(_ webPage: WebPage) -> WebViewController {
        let view = WebViewController()
        let presenter = WebPresenter()
        let router = WebRouter()
        let interactor = WebInteractor()

        //let navigation = UINavigationController(rootViewController: view!)

        view.presenter =  presenter

        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.webPage = webPage

        interactor.output = presenter
        
        router.viewController = view

        //return navigation

        return view
    }
}

extension WebRouter: WebWireframe {
    // TODO: Implement wireframe methods
}
