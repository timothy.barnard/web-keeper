//
//  WebViewController.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class WebViewController: BackButtonViewController {
    
    // MARK: Properties
    var presenter: WebPresentation?
    var webView: WKWebView?
    var configuration = WKWebViewConfiguration()
    var webURL: String = "https://timothybarnard.org"
    lazy var progressView: UIProgressView = {
        let progressView  = UIProgressView(frame: .zero)
        progressView.progress = 0
        progressView.translatesAutoresizingMaskIntoConstraints = false
        return progressView
    }()
    

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        presenter?.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.viewDidAppear()
    }

    private func setupView() {
        self.view.backgroundColor = .white
        let pool = WKProcessPool()
        self.configuration = WKWebViewConfiguration()
        self.configuration.processPool = pool
        
        webView = WKWebView(frame: view.bounds, configuration: self.configuration)
        guard let webview = self.webView else {return}
        webview.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(webview)
        NSLayoutConstraint.activate([
            webview.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0),
            webview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            webview.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            webview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            ])
        
        view.addSubview(self.progressView)
        NSLayoutConstraint.activate([
            self.progressView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.progressView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.progressView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.progressView.heightAnchor.constraint(equalToConstant: 20),
            ])
        
        webview.navigationDelegate = self
        webview.uiDelegate = self
        webview.addObserver(self, forKeyPath: #keyPath(WKWebView.title), options: .new, context: nil)
        webview.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "title" {
            if let title = webView?.title {
                print(title)
            }
        } else if keyPath == "estimatedProgress", let progress = webView?.estimatedProgress {
            self.progressView.progress = Float(progress)
        }
    }
}

extension WebViewController: WebView {
    
    func showURL(_ urlString: String) {
        self.webURL = urlString
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        self.webView?.load(request)
    }
}

extension WebViewController: WKNavigationDelegate {
    
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//        if let url = navigationAction.request.url {
//            if url.host == self.webURL.replacingOccurrences(of: "https://", with: "") {
//                decisionHandler(.allow)
//                return
//            }
//        }
//
//        decisionHandler(.cancel)
//    }
}

extension WebViewController: WKUIDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.progressView.removeFromSuperview()
    }
}
