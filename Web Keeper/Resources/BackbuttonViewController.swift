//
//  BackbuttonViewController.swift
//  Web Keeper
//
//  Created by Timothy on 17/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

class BackButtonViewController: UIViewController {
    
    private lazy var backButton: UIButton = {
        let button = RoundEdgesButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        button.setImage(#imageLiteral(resourceName: "back-button"), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(goBack(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupView()
    }
    
    private func setupView() {
        self.view.addSubview(backButton)
        NSLayoutConstraint.activate([
            backButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -50),
            backButton.widthAnchor.constraint(equalToConstant: 60),
            backButton.heightAnchor.constraint(equalToConstant: 60),
            backButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20),
            ])
    }
    
    @objc private func goBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
