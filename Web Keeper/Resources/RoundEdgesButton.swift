//
//  RoundEdgesButton.swift
//  Web Keeper
//
//  Created by Timothy on 17/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit

class RoundEdgesButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    private func setupLayout() {
        addShadow()
    }
    
}
