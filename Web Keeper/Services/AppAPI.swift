//
//  AppAPI.swift
//  Web Keeper
//
//  Created by Timothy on 17/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import SwiftyNetworking

enum NetworkEnvironment {
    case qa
    case production
    case staging
}

public enum AppAPI {
    case favIcon(String)
}

extension AppAPI: EndPointType {
    
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .production, .qa, .staging: return "https://timothybarnard.org"
        }
    }
    
    public var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    public var path: String {
        switch self {
        case .favIcon: return "api/v2/cms/favicon/"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .favIcon: return .post
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .favIcon(let url):
            let dict = ["url": url]
            return .requestParameters(bodyParameters: dict,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: nil)
        }
    }
    
    public var headers: HTTPHeaders? {
        return nil
    }
}
