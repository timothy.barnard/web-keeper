//
//  CloudKitManager.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit
import CloudKit

class CloudKitManager {
    
    private let database = CKContainer.default().privateCloudDatabase
    
    func saveNewObject(_ model: CloudModel, name: String, blobURL: String?) {
        let newObject = CKRecord(recordType: name)
        newObject.setValuesForKeys(model.getDict())
        if let urlString = blobURL, let url = URL(string: urlString) {
            do {
                guard let data = try? Data(contentsOf: url), let image =  UIImage(data: data) else {return}
                let asset = try CKAsset(image:image)
                newObject["image"] = asset
            }
            catch {
                print("Error creating assets", error)
            }
        }
        self.database.save(newObject) { (record, error) in
            if let error = error {
                print(error)
            }
            guard record != nil else { return}
            print("New object saved")
        }
    }
    
    func getAllObjects(_ name: String, completion: @escaping (_ objects: [CloudModel])->()){
        let query = CKQuery(recordType: name, predicate: NSPredicate(value: true))
        self.database.perform(query, inZoneWith: nil) { records, error in
            DispatchQueue.main.async {
                guard let records = records else {return}
                let webPages = records.map({ record -> CloudModel in
                    let dict = [
                        "title": record["title"] as Any,
                        "url": record["url"] as Any,
                        "imagePath": record["imagePath"] as Any
                    ]
                    let webPage = WebPage.convert(dict) as! WebPage
                    if let asset = record["image"] as? CKAsset,
                        let data = NSData(contentsOf: asset.fileURL),
                        let image = UIImage(data: data as Data) {
                        webPage.image = image
                    }
                    return webPage
                })
                completion(webPages)
            }
        }
    }
}
