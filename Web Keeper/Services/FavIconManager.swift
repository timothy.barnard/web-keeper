//
//  FavIconManager.swift
//  Web Keeper
//
//  Created by Timothy on 17/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import SwiftyNetworking
import SwiftyExtensions

class FavIconManager {
    static func retrieveFavIcon( _ url: String, retrievedFoodItem : @escaping (_ result: Result, _ favIcons: [FavIcon]?) -> Void) {
        let networkManager = NetworkManager()
        networkManager.request(endPoint: .favIcon(url)) { (data, error, result) in
            let favIconResult = result ?? .failure(-1)
            guard let data = data else {
                retrievedFoodItem(favIconResult, nil)
                return
            }
            if let favIcons: [FavIcon] = FavIcon.encodeWrapperArray(data, format: "YYYY-MM-DD") {
                retrievedFoodItem(favIconResult, favIcons)
            } else {
                retrievedFoodItem(favIconResult, nil)
            }
        }
    }
}
