//
//  NetworkManager.swift
//  Web Keeper
//
//  Created by Timothy on 17/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import SwiftyNetworking

struct NetworkManager {
    static let environment = NetworkEnvironment.production
    static let APIAccessToken = "2c72f125-6bcf-41da-93b1-63f88ff30848"
    let router = Router<AppAPI>()
    func request(endPoint: AppAPI, completion: @escaping (_ data: Data?,_ error: Error?,_ result: Result?) -> Void ) {
        router.request(endPoint) { (data, error, result) in
            completion(data, error, result)
        }
    }
}
