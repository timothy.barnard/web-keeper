//
//  WebPage.swift
//  Web Keeper
//
//  Created by Timothy on 16/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit
import CloudKit

protocol CloudModel {
    func getDict() -> [String: Any]
    static func convert(_ dict: [String: Any]) -> CloudModel
    static var name: String {get}
}

class WebPage: CloudModel {
    var title: String = ""
    var url: String = ""
    var imagePath: String = ""
    var image: UIImage?
    
    func getDict() -> [String : Any] {
        return [
            "title": self.title,
            "url": self.url,
            "imagePath": self.imagePath
        ]
    }
    
    static var name: String {
        return "WebPage"
    }
    
    static func convert(_ dict: [String: Any]) -> CloudModel {
        let newWebPage = WebPage()
        if let title = dict["title"] as? String {
            newWebPage.title = title
        }
        if let urlValue = dict["url"] as? String {
            newWebPage.url = urlValue
        }
        if let imagePath = dict["imagePath"] as? String {
            newWebPage.imagePath = imagePath
        }
        return newWebPage
    }
}
